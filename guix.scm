;;; SPDX-FileCopyrightText: © 2022 Maik Herbers
;;; SPDX-License-Identifier: GPL-3.0-or-later

(use-modules (gnu packages image)
             (gnu packages multiprecision)
             (gnu packages pkg-config)

             (guix build-system meson)
             (guix gexp)
             (guix licenses)
             (guix packages)

             (ice-9 popen)
             (ice-9 rdelim))

(define polyplot
  (let*
      ((%source-dir (dirname (current-filename)))
       (cmd (string-append "git -C " %source-dir " describe --always"))
       (port (open-pipe cmd OPEN_READ))
       (%git-version (read-line port))
       (select? (lambda (name _)
                  (not (string=? name
                                 (string-append %source-dir "/.git"))))))
    (close port)
    (package
     (name "polyplot")
     (version %git-version)
     (source (local-file %source-dir
                         #:recursive? #t
                         #:select? select?))
     (build-system meson-build-system)
     (arguments (list #:tests? #f))
     (inputs (list libpng mpc mpfr))
     (native-inputs (list pkg-config))
     (synopsis "Plotter for complex power series")
     (description "This package creates plots of power series on their domain of
convergence. It uses the perceptually uniform colormap @code{cividis}
to represent phase.")
     (home-page (string-append "file://localhost" %source-dir))
     (license gpl3+))))

polyplot
