/* SPDX-FileCopyrightText: © 2022 Maik Herbers */
/* SPDX-License-Identifier: GPL-3.0-or-later */

#pragma once

#include <png.h>

#include "common.h"

struct pic {
	png_image im;
	struct rgba_pix *pix;
};

struct pic init_pic(uint64_t height, uint64_t width);
void save_to_png(struct pic pic, const char *out);
void free_pic(struct pic pic);
