/* SPDX-FileCopyrightText: © 2022 Maik Herbers */
/* SPDX-License-Identifier: GPL-3.0-or-later */

#pragma once

#include "common.h"

struct polynomial {
	uint64_t degree;
	mpc_t *coeffs;
};

void horner_eval(mpc_t res, const mpc_t z, const struct polynomial p);
