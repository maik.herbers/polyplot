/* SPDX-FileCopyrightText: © 2022 Maik Herbers */
/* SPDX-License-Identifier: GPL-3.0-or-later */

#pragma once

#include <stdint.h>

#include <mpc.h>
#include <mpfr.h>

struct rgba_pix {
	uint8_t r;
	uint8_t g;
	uint8_t b;
	uint8_t a;
};

struct temporaries {
	mpfr_t pi;
	mpfr_t mpfr;
	mpc_t mpc;
};

struct temporaries init_temporaries(uint64_t prec);
void destroy_temporaries(struct temporaries tmps);
