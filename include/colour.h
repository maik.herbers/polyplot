/* SPDX-FileCopyrightText: © 2022 Maik Herbers */
/* SPDX-License-Identifier: GPL-3.0-or-later */

#pragma once

#include "common.h"

enum colour_type {
	colour_type_mono,
	colour_type_cividis,
	colour_type_viridis,
};

typedef void (*colour_function_t)(struct rgba_pix *, const mpc_t, struct temporaries, enum colour_type type);

void get_colour_mono(struct rgba_pix *colour, const mpc_t val, struct temporaries tmps, enum colour_type type);
void get_colour_cmap(struct rgba_pix *colour, const mpc_t val, struct temporaries tmps, enum colour_type type);
