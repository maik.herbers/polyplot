/* SPDX-FileCopyrightText: © 2022 Maik Herbers */
/* SPDX-License-Identifier: GPL-3.0-or-later */

#include "common.h"

struct temporaries init_temporaries(uint64_t prec)
{
	struct temporaries tmps = { 0 };

	mpfr_init2(tmps.pi, prec);
	mpfr_const_pi(tmps.pi, MPFR_RNDN);
	mpfr_init2(tmps.mpfr, prec);
	mpc_init2(tmps.mpc, prec);

	return tmps;
}

void destroy_temporaries(struct temporaries tmps)
{
	mpfr_clear(tmps.pi);
	mpfr_clear(tmps.mpfr);
	mpc_clear(tmps.mpc);
}
