/* SPDX-FileCopyrightText: © 2022 Maik Herbers */
/* SPDX-License-Identifier: GPL-3.0-or-later */

#include "colour.h"

#include "cividis_lut.h"
#include "viridis_lut.h"

void get_colour_mono(struct rgba_pix *colour, const mpc_t val, struct temporaries tmps, enum colour_type type)
{
	// only needed so that the signature matches
	(void)type;

	/*
	 * Use
	 *         1/π * arctan(log(|val|^α + 1)) ∈ [0, 1]
	 * as brightness.
	 *
	 * See [0] section 2.2.2 for more.
	 *
	 * [0]: https://arxiv.org/abs/2002.05234
	 */
	mpc_abs(tmps.mpfr, val, MPFR_RNDN);
	mpfr_rootn_ui(tmps.mpfr, tmps.mpfr, 4UL, MPFR_RNDN);
	mpfr_log(tmps.mpfr, tmps.mpfr, MPFR_RNDN);
	mpfr_add_ui(tmps.mpfr, tmps.mpfr, 1UL, MPFR_RNDN);
	mpfr_atan(tmps.mpfr, tmps.mpfr, MPFR_RNDN);

	mpfr_div(tmps.mpfr, tmps.mpfr, tmps.pi, MPFR_RNDN);
	mpfr_add_d(tmps.mpfr, tmps.mpfr, 0.5, MPFR_RNDN);

	// get brightness in steps of 1/256-ths
	mpfr_mul_ui(tmps.mpfr, tmps.mpfr, 256, MPFR_RNDN);
	uint8_t brightness = mpfr_get_ui(tmps.mpfr, MPFR_RNDZ);
	*colour = (struct rgba_pix){ brightness, brightness, brightness, 0xff };
}

inline static struct rgba_pix interpolate(double t, struct rgba_pix a, struct rgba_pix b)
{
	return (struct rgba_pix){
		(double)a.r + t * ((double)b.r - (double)(b.r)),
		(double)a.g + t * ((double)b.g - (double)(b.g)),
		(double)a.b + t * ((double)b.b - (double)(b.b)),
		0xff
	};
}

void get_colour_cmap(struct rgba_pix *colour, const mpc_t val, struct temporaries tmps, enum colour_type type)
{
	struct rgba_pix *lut = (type == colour_type_cividis ? cividis_lut : viridis_lut);

	mpc_arg(tmps.mpfr, val, MPFR_RNDN);
	mpfr_div(tmps.mpfr, tmps.mpfr, tmps.pi, MPFR_RNDN);
	mpfr_mul_ui(tmps.mpfr, tmps.mpfr, 256, MPFR_RNDN);
	uint8_t idx = mpfr_get_ui(tmps.mpfr, MPFR_RNDZ);

	if (idx == 0xff) {
		*colour = lut[idx];
		return;
	}

	// interpolate linearly
	mpfr_frac(tmps.mpfr, tmps.mpfr, MPFR_RNDN);
	double t = mpfr_get_d(tmps.mpfr, MPFR_RNDN);

	*colour = interpolate(t, lut[idx], lut[idx + 1]);
}
