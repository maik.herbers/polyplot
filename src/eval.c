/* SPDX-FileCopyrightText: © 2022 Maik Herbers */
/* SPDX-License-Identifier: GPL-3.0-or-later */

#include "eval.h"

void horner_eval(mpc_t res, const mpc_t z, const struct polynomial p)
{
	mpc_set(res, p.coeffs[p.degree], MPFR_RNDN);

	for (uint64_t i = p.degree; i > 0; i--) {
		mpc_mul(res, res, z, MPFR_RNDN);
		mpc_add(res, res, p.coeffs[i - 1], MPFR_RNDN);
	}
}
