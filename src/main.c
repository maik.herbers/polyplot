/* SPDX-FileCopyrightText: © 2022 Maik Herbers */
/* SPDX-License-Identifier: GPL-3.0-or-later */

#include <stdbool.h>
#include <stdlib.h>

#include <ctype.h>
#include <errno.h>
#include <string.h>

#include <fcntl.h>
#include <unistd.h>

#include "common.h"
#include "colour.h"
#include "eval.h"
#include "png_simplified.h"

#include "config.h"

#define POLY_SIZE 20

struct options {
	uint64_t prec;
	uint64_t resolution;
	enum colour_type colour_type;

	mpc_t offset;
	mpc_t x_step;
	mpc_t y_step;
	mpfr_t radius;
};

static inline void get_pos(const struct options opts, mpc_t z, uint64_t i, uint64_t j, struct temporaries tmps)
{
	mpc_set(z, opts.offset, MPFR_RNDN);
	mpc_mul_ui(tmps.mpc, opts.y_step, i, MPFR_RNDN);
	mpc_add(z, z, tmps.mpc, MPFR_RNDN);
	mpc_mul_ui(tmps.mpc, opts.x_step, j, MPFR_RNDN);
	mpc_add(z, z, tmps.mpc, MPFR_RNDN);
}

struct options init_options(enum colour_type type, uint64_t prec, uint64_t resolution)
{
	struct options opts = { 0 };

	opts.prec = prec;
	opts.resolution = resolution;
	opts.colour_type = type;

	mpc_init2(opts.offset, prec);
	mpc_init2(opts.x_step, prec);
	mpc_init2(opts.y_step, prec);
	mpfr_init2(opts.radius, prec);

	return opts;
}

void destroy_options(struct options opts)
{
	mpc_clear(opts.offset);
	mpc_clear(opts.x_step);
	mpc_clear(opts.y_step);
	mpfr_clear(opts.radius);
}

int get_uint(const char *s, uint64_t *n)
{
	char *endptr;
	int64_t n_signed;

	n_signed = strtoll(s, &endptr, 10);
	if (!*optarg || *endptr) {
		return -1;
	}

	if ((n_signed == LLONG_MIN || n_signed == LLONG_MAX) && errno == ERANGE) {
		return -1;
	}

	if (n_signed < 0) {
		return -1;
	}

	*n = n_signed;

	return 0;
}

int read_polynomial(struct options opts, const char *coefficient_file, struct polynomial *p)
{
	int fd = open(coefficient_file, O_RDONLY);
	if (fd < 0) {
		perror("open");
		return -1;
	}

	const uint64_t len = 1 << 12;

	char *buf = calloc(len, sizeof(char));
	if (!buf) {
		perror("calloc");
		return -1;
	}

	uint64_t degree = POLY_SIZE;
	p->coeffs = calloc(degree, sizeof(mpc_t));
	if (!p->coeffs) {
		perror("calloc");
		free(buf);
		return -1;
	}

	off_t off = 0;
	ssize_t n;
	char *endptr;
	uint64_t idx = 0;
	while ((n = pread(fd, buf, len - 1, off)) > 0) {
		if (n == 1 && isspace(buf[0])) {
			break;
		}

		buf[n] = '\0';
		mpc_init2(p->coeffs[idx], opts.prec);
		if (mpc_strtoc(p->coeffs[idx], buf, &endptr, 10, MPFR_RNDN)) {
			fprintf(stderr, "failed to read complex number from `%s'.\nMaybe the precission is too low.", buf);
			for (uint64_t i = 0; i <= idx; i++) {
				mpc_clear(p->coeffs[i]);
			}

			free(p->coeffs);

			free(buf);

			close(fd);

			return -1;
		}

		off += endptr - buf;
		idx++;

		if (idx == degree) {
			mpc_t *tmp = realloc(p->coeffs, (degree + POLY_SIZE) * sizeof(mpc_t));
			if (!tmp) {
				perror("realloc");
				for (uint64_t i = 0; i < degree; i++) {
					mpc_clear(p->coeffs[i]);
				}

				free(p->coeffs);

				free(buf);

				close(fd);

				return -1;
			}

			p->coeffs = tmp;
			degree += POLY_SIZE;
		}
	}

	if (errno) {
		perror("pread");
		for (uint64_t i = 0; i < idx; i++) {
			mpc_clear(p->coeffs[i]);
		}

		free(p->coeffs);

		free(buf);

		close(fd);

		return -1;
	}

	if (idx == 0) {
		fprintf(stderr, "coefficient file `%s' is empty\n", coefficient_file);
		for (uint64_t i = 0; i < idx; i++) {
			mpc_clear(p->coeffs[i]);
		}

		free(p->coeffs);

		free(buf);

		close(fd);

		return -1;
	}

	p->degree = idx - 1;

	free(buf);

	close(fd);

	return 0;
}

void destroy_polynomial(struct polynomial p)
{
	for (uint64_t i = 0; i <= p.degree; i++) {
		mpc_clear(p.coeffs[i]);
	}

	free(p.coeffs);
}

void fill(struct pic pic, struct options opts, const struct polynomial p)
{
	colour_function_t get_colour = (opts.colour_type == colour_type_mono ? &get_colour_mono : &get_colour_cmap);
#pragma omp parallel
	{
		struct temporaries tmps = init_temporaries(opts.prec);

		mpc_t z;
		mpc_init2(z, opts.prec);
		mpc_t val;
		mpc_init2(val, opts.prec);

#pragma omp for
		for (uint64_t i = 0; i < opts.resolution; i++) {
			for (uint64_t j = 0; j < opts.resolution; j++) {
				get_pos(opts, z, i, j, tmps);

				mpc_abs(tmps.mpfr, z, MPFR_RNDN);
				if (mpfr_greater_p(tmps.mpfr, opts.radius)) {
					pic.pix[i * opts.resolution + j] = (struct rgba_pix){ 0, 0, 0, 0 };
					continue;
				}

				horner_eval(val, z, p);
				get_colour(pic.pix + i * opts.resolution + j, val, tmps, opts.colour_type);
			}
		}

		destroy_temporaries(tmps);

		mpc_clear(z);
		mpc_clear(val);

		mpfr_free_cache2(MPFR_FREE_LOCAL_CACHE);
	}
}

void print_help(const char *exe)
{
	printf(
		"Usage:\n"
		"\t%s [options] <coefficient_file>\n"
		"\n"
		"Options:\n"
		"  -h              show this help message\n"
		"  -V              show version information\n"
		"  -t TYPE         set colour type, one of mono, cividis, viridis\n"
		"  -p PREC         set precision\n"
		"  -c CENTER       set center\n"
		"  -r RADIUS       set radius\n"
		"  -R RESOLUTION   set resolution\n"
		"  -o OUTFILE      set output file\n",
		exe);
}

void print_version(void)
{
	printf(
		"polyplot version %s\n"
		"Copyright (C) 2022  Maik Herbers\n"
		"\n"
		"This program is free software: you can redistribute it and/or modify\n"
		"it under the terms of the GNU General Public License as published by\n"
		"the Free Software Foundation, either version 3 of the License, or\n"
		"(at your option) any later version.\n"
		"\n"
		"This program is distributed in the hope that it will be useful,\n"
		"but WITHOUT ANY WARRANTY; without even the implied warranty of\n"
		"MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n"
		"GNU General Public License for more details.\n"
		"\n"
		"You should have received a copy of the GNU General Public License\n"
		"along with this program.  If not, see <https://www.gnu.org/licenses/>.\n",
		POLYPLOT_VERSION);
}

int main(int argc, char **argv)
{
	uint64_t prec = 53UL;
	uint64_t resolution = 100UL;

	const char *center_str = "(0 0)";
	const char *radius_str = "1";
	const char *out_file = "out.png";
	const char *coefficient_file = "";

	enum colour_type colour_type = colour_type_cividis;

	struct options opts;

	while (true) {
		switch (getopt(argc, argv, "+hVt:p:c:r:R:o:")) {
		case -1:
			goto escape;
		case 'h':
			print_help(argv[0]);

			return EXIT_SUCCESS;
		case 't':
			if (strcmp("mono", optarg) == 0) {
				colour_type = colour_type_mono;
			} else if (strcmp("cividis", optarg) == 0) {
				colour_type = colour_type_cividis;
			} else if (strcmp("viridis", optarg) == 0) {
				colour_type = colour_type_viridis;
			} else {
				printf("invalid colour type `%s'\n", optarg);

				return EXIT_FAILURE;
			}

			break;
		case 'p':
			if (get_uint(optarg, &prec)) {
				printf("invalid precision `%s'\n", optarg);
				return EXIT_FAILURE;
			}

			if (prec < MPFR_PREC_MIN || prec > MPFR_PREC_MAX) {
				printf("precision `%lu' outside of allowed range [%d, %ld]\n", prec, MPFR_PREC_MIN, MPFR_PREC_MAX);
				return EXIT_FAILURE;
			}

			break;
		case 'c':
			center_str = optarg;

			break;
		case 'r':
			radius_str = optarg;

			break;
		case 'R':
			if (get_uint(optarg, &resolution)) {
				printf("invalid resolution `%s'\n", optarg);
				return EXIT_FAILURE;
			}

			break;
		case 'o':
			out_file = optarg;

			break;
		case 'V':
			print_version();

			return EXIT_SUCCESS;
		case '?':
		default:
			print_help(argv[0]);

			return EXIT_FAILURE;
		}
	}

escape:
	if (argc - optind != 1) {
		print_help(argv[0]);
		return EXIT_FAILURE;
	}

	coefficient_file = argv[optind];

	mpc_t tmp;
	mpc_t center;

	mpc_init2(tmp, prec);
	mpc_init2(center, prec);

	opts = init_options(colour_type, prec, resolution);

	if (mpc_set_str(center, center_str, 10, MPFR_RNDN)) {
		printf("invalid center argument `%s'\n", center_str);
		return EXIT_FAILURE;
	}

	if (mpfr_set_str(opts.radius, radius_str, 10, MPFR_RNDN)) {
		printf("invalid radius argument `%s'\n", radius_str);
		return EXIT_FAILURE;
	}

	if (!mpfr_regular_p(opts.radius)) {
		printf("invalid radius `%s'\n", center_str);
		return EXIT_FAILURE;
	}

	mpc_set_fr_fr(tmp, opts.radius, opts.radius, MPFR_RNDN);
	mpc_sub(opts.offset, center, tmp, MPFR_RNDN);

	mpc_set_fr(opts.x_step, opts.radius, MPFR_RNDN);
	mpc_div_ui(opts.x_step, opts.x_step, resolution, MPFR_RNDN);
	mpc_mul_ui(opts.x_step, opts.x_step, 2, MPFR_RNDN);
	mpc_mul_i(opts.y_step, opts.x_step, 1, MPFR_RNDN);

	mpc_clear(tmp);
	mpc_clear(center);

	struct pic pic = init_pic(opts.resolution, opts.resolution);
	if (!pic.pix) {
		perror("calloc");
		return EXIT_FAILURE;
	}

	struct polynomial p;
	if (read_polynomial(opts, coefficient_file, &p)) {
		return EXIT_FAILURE;
	}

	fill(pic, opts, p);

	destroy_polynomial(p);

	destroy_options(opts);

	mpfr_free_cache();

	save_to_png(pic, out_file);

	free_pic(pic);

	return EXIT_SUCCESS;
}
