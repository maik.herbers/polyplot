/* SPDX-FileCopyrightText: © 2022 Maik Herbers */
/* SPDX-License-Identifier: GPL-3.0-or-later */

#include "png_simplified.h"

#include <stdlib.h>

struct pic init_pic(uint64_t height, uint64_t width)
{
	png_image im = {
		.version = PNG_IMAGE_VERSION,
		.height = height,
		.width = width,
		.format = PNG_FORMAT_RGBA,

		.opaque = NULL,
		.flags = 0
	};

	struct pic pic = {
		.im = im,
		.pix = NULL
	};

	struct rgba_pix *pix = calloc(sizeof(struct rgba_pix), height * width);
	if (!pix) {
		return pic;
	}

	pic.pix = pix;

	return pic;
}

void save_to_png(struct pic pic, const char *out)
{
	png_image_write_to_file(&pic.im, out, 0, pic.pix, 0, NULL);
}

void free_pic(struct pic pic)
{
	free(pic.pix);
}
